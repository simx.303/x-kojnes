from enum import Enum

"""Terminai"""

class Inv(Enum):
    """Inventorius"""
    BLOCKS = "blocks"
    MOKEJIMAI = "mks"

class Kom(Enum):
    """Komandos"""
    INVENTORIUS = "inv"
    GAUTIBLOKUS = "getblocks"
    GAUTIMOKEJIMUS = "getmks"
    GAUTIDUOMENIS = "getdata"
    PRASYTIMAZGU = "getpeers"
    MAZGAI = "peers"
    MSG = "msg"
    BLOKAI = "blocks"
    MOKEJIMAI = "mks"
