import imp
import time
from zlib import crc32
import json
import os
from protokolai import makalas, blokui


BLOCKS_DIR = "./blokai/%s"

if not os.path.exists(BLOCKS_DIR % ""):
    os.mkdir(BLOCKS_DIR%"")

        


class Block:

    def __init__(self):
        self.created = int(time.time())
        self.id = self.getid()
        self.nonce = 0
        self.merkelroot = ""
        self.lastHash = self.getlHash()
        self.transactions = {}

    @property    
    def header(self):
        return {'id':self.id,'nonce':self.nonce,'merkelroot':self.merkelroot,'lastHash':self.lastHash,'created':self.created}

    # def calcMerkelroot(self):
    #     nodes = 
    #     for i in self.transactions:
    #         makalas

    def setFromFile(self,file):
        fp = json.load(open(file,'r'))
        self.created = fp['created']
        self.id = fp['id']
        self.nonce = fp['nonce']
        self.transactions = fp['transactions']
        self.lastHash = fp['lastHash']

    def getid(self):
        blocks = os.listdir(BLOCKS_DIR%"")
        if blocks == []:
            return 0
        else:
            return max([int(f) for f in blocks])+1
    def getlHash(self):
        if self.id == 0:
            return 382742000
        else:
            return blokui(BLOCKS_DIR % str(self.id-1))
    def addTransactions(self, transactions):
        for tx in transactions:
            self.transactions[makalas(tx)]=tx
    
    def dump(self):
        json.dump(self.__dict__,open(BLOCKS_DIR % str(self.id),'w'))
    def dumps(self):
        return json.dumps(self.__dict__)

    def getSelf(self):
        return self.__dict__

