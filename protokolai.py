import hashlib
from zlib import crc32
import json
# import Block


def makalas(duom):
    return hashlib.sha256(bytes(str(duom),"utf-8")).hexdigest()

def merkel(daiktai:list) -> str:
    lapai=[makalas(i) for i in daiktai]
    while len(lapai)>1:
        ro=[]
        if not len(lapai)%2==0:
            lapai.append(lapai[-1])
        while lapai!=[]:
            ro.append(makalas(lapai.pop(0)+lapai.pop(0)))
        lapai=ro
    return lapai[0]

def baze3(sk:int) -> str:
    xs = "abc"
    baze = len(xs)
    (daliklis, liekana) = (1,0)
    rez = ""
    if baze==1:
        return xs * sk
    while daliklis > 0:
        (daliklis, liekana) = divmod(sk, baze)
        sk = daliklis
        rez = xs[liekana] + rez
    return rez

def blokui(blokas) -> int:
    if isinstance(blokas,str):
        return crc32(bytes(str(json.load(open(blokas,"r"))),'utf-8'))
    elif isinstance(blokas,dict):
        return crc32(bytes(str(blokas),'utf-8'))
    # elif isinstance(blokas,Block):
    #     return crc32(bytes(str(blokas.getSelf()),'utf-8'))