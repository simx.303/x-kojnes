from p2pnetwork.node import Node
import Block
from miner import Miner
from terminai import *
import json
import protokolai as pkol
from socket import gethostname

import time, os
import hashlib

import configparser
config = configparser.ConfigParser()
if not os.path.exists('pakas.ini'):
    with open('pakas.ini', 'w') as fil:
        conf = config['PAGR']
        conf['port'] = 5894
        conf['debug'] = False
        conf['msg_del_time'] = 30
        conf['blocksdir'] = './blokai/'
        conf['host'] = gethostname()
        config.write(fil)
        fil.close()

config.read("pakas.ini")
conf = config['PAGR']
print(conf['msg_del_time'])

miner = Miner()


class Mazgas (Node):

    # Python class constructor
    def __init__(self, host, port, id=None, callback=None, max_connections=5):
        super(Mazgas, self).__init__(host, port, id, callback, max_connections)
        
        self.requested_nodes = set()
        self.mokejimai = dict()
        self.neturim = {Inv.BLOCKS:[],Inv.MOKEJIMAI:[]}
        self.blocks = []
        self.announced_msgs= {}


    def save_nodes(self):
        with open("peers.txt","w+") as fil:
            sajunga=fil.read().splitlines()+[i.host+":"+str(i.port) for i in self.all_nodes]
            fil.write("\n".join(set(sajunga)))
            fil.close()

    def message(self, type, data:dict, overides={}):
        # time that the message was sent
        dict = {"type": type, "data": data}
        if "time" not in dict:
            dict["time"] = str(time.time())

        # if "snid" not in dict:
        #     # sender node id
        #     dict["snid"] = str(self.id)

        # if "rnid" not in dict:
        #     # reciever node id
        #     dict["rnid"] = None

        # if "sig" not in dict:
        #     dict["sig"] = cf.sign(data, self.private_key)

        dict = {**dict, **overides}
        return dict
    
    def inv(self,of_what:Inv,hs=None)->dict:
        inventorius = {}
        if Inv.BLOCKS == of_what:
            inventorius[Inv.BLOCKS]=[hs] if hs else list(self.blocks)
        if Inv.MOKEJIMAI == of_what:
            inventorius[Inv.MOKEJIMAI]=[hs] if hs else self.mokejimai
        print(inventorius)
        return self.message(Kom.INVENTORIUS,data=inventorius)
    def gaut_duomenis(self,node,of_what:Inv):
        self.msg(node, self.message(Kom.GAUTIDUOMENIS,{"type":of_what,"query":self.neturim[of_what]}))
    def shout(self, dta, exlude: list=[]):
        for i in self.announced_msgs.copy():
            if time.time() - self.announced_msgs[i] > float(conf.get("msg_del_time",30)):
                del self.announced_msgs[i]
        sth = str(dta)
        hash_object = hashlib.md5(sth.encode("utf-8"))
        msghash = str(hash_object.hexdigest())
        if msghash not in self.announced_msgs:
            self.send_to_nodes(dta, exlude)
            self.announced_msgs[msghash] = time.time()
    def msg(self,node,dta):
        self.send_to_node(node,dta)
    def iskasem(self,hs,blockjson):
        with open(Block.BLOCKS_DIR % "/"+str(hs),"w") as fil:
            fil.write(blockjson)
        self.shout(self.inv(Inv.BLOCKS,hs))

    def outbound_node_connected(self, node):
        print("outbound_connected:  " + node.id)
        self.save_nodes()
        
    def inbound_node_connected(self, node):
        print("inbound_connected: " + node.id)
        self.save_nodes()

    def inbound_node_disconnected(self, node):
        print("inbound_disconnected: " + node.id)

    def outbound_node_disconnected(self, node):
        print("outbound_disconnected: " + node.id)

    def node_message(self, node, data):
        if not isinstance(data,dict):
            self.debug_print("Kazkokia huine")
            return
        komanda=data["type"]
        duom = data["data"]
        if komanda == Kom.MSG:
            print(node.host + ": " + duom)
        elif komanda == Kom.GAUTIMOKEJIMUS:
            self.msg(node,self.inv(Inv.MOKEJIMAI))
        elif komanda == Kom.GAUTIBLOKUS:
            self.msg(node,self.inv(Inv.BLOCKS))
        elif komanda == Kom.GAUTIDUOMENIS:
            query = duom["query"]
            if duom["type"] == Inv.BLOCKS:
                for q in query:
                    if q not in self.blocks:
                        self.msg(node,self.message("notfound",q))
                        return
                    self.msg(node,self.message(Kom.BLOKAI,{"hash":q,Inv.BLOCKS:json.load(open(conf['blocksdir']+q,"r"))}))
            elif duom["type"] == Inv.MOKEJIMAI:
                for q in query:
                    if q not in self.mokejimai:
                        self.msg(node,self.message("notfound",q))
                        return
                    self.msg(node,self.message(Kom.MOKEJIMAI,{"hash":q,Inv.MOKEJIMAI:self.mokejimai[q]}))
        elif komanda == Kom.PRASYTIMAZGU:
            self.msg(node,self.message(Kom.MAZGAI,[{"host":i.host ,"port":int(i.port)} for i in self.all_nodes]))
        
        elif komanda == Kom.MAZGAI and node in self.requested_nodes:
            self.requested_nodes.remove(node)
            print(data)
            for i in duom:
                self.connect_with_node(i["host"],int(i["port"]))
        elif komanda == Kom.INVENTORIUS:
            neturim = {Inv.BLOCKS:[],Inv.MOKEJIMAI:[]}
            if Inv.BLOCKS in duom:
                for block in duom[Inv.BLOCKS]:
                    if block not in self.blocks:
                        self.neturim[Inv.BLOCKS].append(block)
                self.gaut_duomenis(node,Inv.BLOCKS)
            if Inv.MOKEJIMAI in duom:
                for mk in duom[Inv.MOKEJIMAI]:
                    if mk not in self.mokejimai:
                        self.neturim[Inv.MOKEJIMAI].append(mk)
            
        elif komanda == Kom.MOKEJIMAI:
            print(node.id + "\nMOKEJIMAS: " + duom["hash"])
            if duom["hash"] == pkol.makalas(duom[Inv.MOKEJIMAI]):
                self.mokejimai[duom["hash"]]=duom[Inv.MOKEJIMAI]
                self.shout(self.inv(Inv.MOKEJIMAI,hs=duom["hash"]))
                miner.new_transaction(duom["hash"],self.mokejimai[duom["hash"]])
            else:
                print("Netiko")
        elif komanda == Kom.BLOKAI:
            print(node.id + "\nBLOKAS: " + str(duom["hash"]))
            if duom["hash"] not in self.blocks:
                if duom["hash"] == str(pkol.blokui(duom[Inv.BLOCKS])):
                    json.dump(duom[Inv.BLOCKS],open(conf["blocksdir"]+duom["hash"],"w"))
                    self.shout(self.inv(Inv.BLOCKS,hs=duom["hash"]))
                    mined_tx=[pkol.makalas(tx) for tx in duom[Inv.BLOCKS]["transactions"]]
                    miner.mined_transactions(mined_tx)
                    for tx in duom[Inv.BLOCKS]["transactions"]:
                        self.mokejimai.pop(tx)
                else:
                    print("Netiko")           
        
    def node_disconnect_with_outbound_node(self, node):
        print("node wants to disconnect with oher outbound node: (" + self.id + "): " + node.id)
        
    def node_request_to_stop(self):
        print("node is requested to stop (" + self.id + "): ")
    
    def request_peers(self, max: int=5):
        for i,n in enumerate(self.all_nodes):
            if i < max:
                self.send_to_node(n,{"type":"GETpeers"})
                self.requested_nodes.update([n])
    def gauti_inv(self, ko:Kom, max: int=5):
        for i,n in enumerate(self.all_nodes):
            if i < max:
                self.send_to_node(n,self.message(ko,""))


debug = bool(conf.get('debug',False))

HOST=conf(['host'],gethostname())
print("KONSOLE HOST: "+HOST)
PORT=5894
try:
    PORT=int(conf['port'])
except:
    PORT=int(input("PORT>"))
mazgas = Mazgas(HOST, PORT, max_connections=4)
miner.main_node=mazgas
mazgas.debug=debug
mazgas.start()

mazgas.blocks = os.listdir(conf["blocksdir"])

if not os.path.exists('mazgai.txt'):
    with open('mazgai.txt', 'w'): pass
with open("mazgai.txt","r") as fil:
    for i in fil.read().splitlines():
        mazgas.connect_with_node(i.split(":")[0],int(i.split(":")[1]))
    fil.close()

mazgas.gauti_inv(Kom.BLOKAI)

try:
    while True:
        cmd: str=input("Mazgas> ")
        if cmd == "help":
            print(
                """
            mk
            connect
            msg
            debug
            stop
            exit
            blok
            peers
            req
            mks
            kasti
            """
            )
        if "connect " in cmd:
            args = cmd.replace("connect ", "")
            print("connect to: " + args.split(":")[0]+" port "+args.split(":")[1])
            mazgas.connect_with_node(args.split(":")[0],int(args.split(":")[1]))

        if "msg " in cmd:
            args = cmd.replace("msg ", "")
            print("sent msg: " + args)
            mazgas.message("msg",args)
        if "blok" in cmd:
            miner.make_template()
        if "kasti" in cmd:
            miner.run()
        if "mk " in cmd:
            args = cmd.replace("mk ", "")
            print("Mokejimas: " + args)
            mazgas.mokejimai[pkol.makalas(args)]=args
            mazgas.message(Kom.INVENTORIUS,pkol.makalas(args))
        if cmd == "stop":
            mazgas.stop()

        if cmd == "debug":
            mazgas.debug = not mazgas.debug
            print("DEBUG:",mazgas.debug)

        if cmd == "exit":
            mazgas.stop()
            exit(0)
        if cmd == "peers":
            print(mazgas.all_nodes)
        if cmd == "req":
            mazgas.request_peers()
        if cmd == "mks":
            mazgas.request_mks()
            print(mazgas.mokejimai)

except Exception as e:
    mazgas.stop()
    raise (e)

print('end')