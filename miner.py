from Block import Block
from zlib import crc32
import json
import os
from terminai import *

# block = Block()
# block.setFromFile("./blocks/"+str(input('Block ID: ')))
# print(block)


import time
import threading
import json

class Miner(threading.Thread):
    """Kasa"""

    def __init__(self):
        """Nepamirsk run()
            `lol`
        """
        self.main_node = None
        self.block=None
        self.min_transactions=1
        self.transactions={}
        self.mining_transactions={}
        self.terminate_flag = threading.Event()
        self.transactions_mined=False

        # Miner id

        # Datastore to store additional information concerning the miner
        self.info = {}

    
    def new_transaction(self,hs,tx):
        self.transactions[hs]=tx
        if len(self.transactions[hs]) >= self.min_transactions:
            self.make_template()
            self.mine()
    def mined_transactions(self,txs):
        for tx in txs:
            if tx in self.mining_transactions:
                self.transactions_mined=True
                self.transactions.remove(tx)
                self.mining_transactions.remove(tx)

    def make_template(self):
        self.mining_transactions = self.transactions
        self.block=Block()
        texs = [tx for self.mining_transactions[tx] in self.mining_transactions]
        self.block.addTransactions(texs)
        print("Blokas kurtas: ",self.block.dumps())
    
    def run(self):
        b = self.block
        sunkumas = 100
        kulverstukas=0
        while not self.transactions_mined and not self.terminate_flag.is_set():
            if crc32(bytes(b.dumps(),'utf-8')) % sunkumas == 0:
                print(kulverstukas,"MUSUU>> ",crc32(bytes(b.dumps(),'utf-8')))
                self.main_node.iskasem(crc32(bytes(b.dumps(),'utf-8')),b.dumps())
                break
            else:
                kulverstukas+=1
                b.nonce = kulverstukas
        self.transactions_mined=False
        print("Blokas iskastas")

    def stop(self):
        """Terminates the connection and the thread is stopped. Stop the node client. Please make sure you join the thread."""
        self.terminate_flag.set()

    def set_info(self, key, value):
        self.info[key] = value

    def get_info(self, key):
        return self.info[key]

